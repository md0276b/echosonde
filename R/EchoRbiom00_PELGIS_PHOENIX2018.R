#*******************************************************************************************************
# A BORD : IMPORT TEMPS REEL DES DONNEES ET AFFICHAGE DANS sIG PELGIS
#*******************************************************************************************************
# Chargement de la librairie EchoR------------------------------
#*************************************************
library(EchoR)

# 0. Definitions des chemins -------------------------------
#*************************************************
donnees2='/run/user/1000/gvfs/smb-share:domain=ifr,server=nantes,share=donnees2,user=mdoray/'
#prefix='C:/Users/mdoray/documents/'
#prefix=paste(donnees2,'Campagnes/',sep='')
#
prefix='M:/'
prefix='G:/'
# prefix='Z:/Campagnes/'

#nom de la campagne dans EchoR
cruise='PHOENIX2018'
#nom de la campagne dans casino
nomCampagneCasino='PHOENIX18'

# chemin pour exporter les donnees vers le SIG
path.pelGIS=paste(prefix,cruise,'/SIG/pelGIS/',cruise,'/Donnees/',sep='')
dir.create(file.path(path.pelGIS), showWarnings = FALSE,recursive = TRUE)
# chemin vers dossier de l export casino evts
path.cas.echant2=paste(prefix,cruise,'/Data/Casino/',sep='')
dir.create(file.path(path.cas.echant2), showWarnings = FALSE,recursive = TRUE)

# 1. Import et echantillonnage de casino ------------------------------
#(pour completer les positions geographiques manquantes dans l'export tutti)
#*************************************************
# auto-casino si sur la thalassa
library(RMySQL)
casino = dbConnect(MySQL(), user='pelgas', password='thalassa', dbname='casino', host='192.168.52.30')
#dbListTables(casino)
cas0 = dbSendQuery(casino, paste('select * from t_cruiseevent',tolower(nomCampagneCasino),sep=''))
casa = fetch(cas0,n=-1)
param0 = dbSendQuery(casino,paste('select * from t_parameter',tolower(nomCampagneCasino),sep=''))
param = fetch(param0,n=-1)
dbDisconnect(casino)
head(param)
# casa=merge(casa,
#            params[,c('CreatedDateTime','TL_CINNA_shipnav_depth','TL_THSAL_01_hydrology_salinity','TL_THSAL_01_hydrology_temp','TL_FLUOR_01_fluorometer_fluor')],
#            by.x='Date',by.y='CreatedDateTime',all.x=TRUE)
# Echantillonnage du fichier casino
cas1=casa[casa$Type!='ACQAUTO',]
cas2=casa[casa$Type=='ACQAUTO',]
dim(casa);dim(cas1);dim(cas2)
names(casa);names(cas1);names(cas2)
head(casa)
head(param)
names(param)
dim(cas2);dim(param)
dim(cas2)
N=dim(cas2)[1]
head(cas2)
# facteur d' echantillonnage : une acquisition auto toutes les Fechant
Fechant=1
cas2s=cas2[seq(1,N,Fechant),]
case=rbind(cas1,cas2s)
#head(case)
case$date=substr(case$Date,1,10)
case$heure=substr(case$Date,12,20)
case$t=strptime(paste(case$date,case$heure),format='%Y-%m-%d %H:%M:%S',tz='GMT')
# 1.1. export du fichier casino echantillonne ----
head(case)
write.table(case,paste(path.pelGIS,paste(cruise,'casinauto','_echant.csv',sep=''),sep=''),
            sep=',',row.names=FALSE)
dh=strptime(paste(case$date,case$heure),format="%Y-%m-%d %H:%M:%OS")

# 1.2. CUFES ------
#*******************************************************
cufes=cas1[cas1$DeviceCode=='CUFIX',]
dim(cufes)
head(cufes)
hist(param$TL_DEBITCUFES_cufes_flow)
# debit moyen CUFES (L/min)
summary(param[param$TL_DEBITCUFES_cufes_flow>400,
              'TL_DEBITCUFES_01_cufes_flow'])
# export du fichier CUFES
write.table(cufes,paste(path.pelGIS,paste(cruise,'positionsCufes','.csv',sep=''),sep=''),
            sep=',',row.names=FALSE)

# charge le fichier resultat cufes et le sauvegarde au format pelGIS
path.rcufes=paste(prefix,cruise,'\\Hydrocufes\\CUFES\\cufes_counts18vir2.txt',sep='')
path.rcufes=paste(prefix,cruise,'\\Hydrocufes\\CUFES\\Zoocam_abondances_Pelgas2018.txt',sep='')
rcufes=read.delim(path.rcufes,row.names=NULL)
head(rcufes)
Ncol=dim(rcufes)[2]
names(rcufes)=names(rcufes)[-seq(1)]
rcufes=rcufes[,-c(1,Ncol)]
rcufes[,-seq(1)]=apply(rcufes[,-seq(1)],2,as.character)
rcufes[,-seq(1)]=apply(rcufes[,-seq(1)],2,as.numeric)
rcufes[,-seq(1)]=apply(rcufes[,-seq(1)],2,na.null)
rcufes[,-seq(1)]=apply(rcufes[,-seq(1)],2,as.numeric)
head(rcufes)
unique(rcufes$Sp10m3)
#rcufes=apply(rcufes,2,gsub,pattern=',',replacement='.')
write.table(rcufes,paste(path.pelGIS,cruise,'resultsCUFES.csv',sep=''),sep=';',
            row.names = FALSE)

# 1.3. Ferrybox -----
#*******************************************************
names(param)
hydroSurface=param[,c('TL_THSAL_hydrology_salinity','TL_THSAL_hydrology_temp',
                      'TL_FERRYBOX_ferrybox_optode_oxygen',
                      'TL_FERRYBOX_ferrybox_seapoint_turb','TL_FERRYBOX_ferrybox_AOA_GreenAlgae',
                      'TL_FERRYBOX_ferrybox_AOA_BlueAlgae','TL_FERRYBOX_ferrybox_AOA_Diatoms',
                      'TL_FERRYBOX_ferrybox_AOA_Cryptophyta','TL_FERRYBOX_ferrybox_AOA_YellowSub',
                      'TL_FERRYBOX_ferrybox_AOA_TotalAlgae','TL_FERRYBOX_ferrybox_SBE45_SeaTemp',
                      'TL_FERRYBOX_ferrybox_SBE45_Sal','TL_FERRYBOX_ferrybox_FB_ph_value',
                      'TL_FERRYBOX_ferrybox_flow_PCO2')]
hist(param$TL_THSAL_hydrology_salinity)
summary(param$TL_THSAL_hydrology_salinity[param$TL_THSAL_hydrology_salinity>10])
Fechant2=20
N2=dim(param)[1]
params=param[seq(1,N2,Fechant2),]
head(params)
head(cas2)
params$CreatedDateTime
params=merge(params,cas2[,c("Date","Latitude","Longitude")],by.x='CreatedDateTime',
             by.y='Date')

# export des parametres en route
write.table(params,paste(path.pelGIS,paste(cruise,'casinoParameters','.csv',sep=''),sep=''),sep=',',row.names=FALSE)
(max(dh))

par(mfrow=c(1,1))
#head(param)

# 1.4. Vent du Jour -----
#*******************************************************
nday=Sys.Date()
params2=param[substr(param$CreatedDateTime,1,10)==nday,]
names(params2)
plot(params2$TL_WEATHER_weather_trueairspeed,ylab='Wind speed (knots)',main=nday)
scatter.smooth(params$TL_WEATHER_weather_trueairspeed,ylab='Wind speed (knots)',main=nday)
abline(h=30,col=2,lty=2,lwd=2)

# 1.5. Bathysondes et hydrobio------
#*******************************************************
ctd=cas1[cas1$DeviceCode=='BATHY'&cas1$ActionCode=='DEBFIL',]
names(ctd)[c(3:5,9:16)]=c('TC','lat','lon','gearName','GEAR','nEVT','EVT',
                          'ID','COM','NumStation','STRATE')
write.table(ctd,paste(prefix,cruise,'/SIG/pelGIS/',cruise,'/Donnees/',cruise,'CTD.csv',sep=''),
            row.names=F,quote=F,sep=";")
unique(cas1$DeviceCode)
unique(cas1$ActionCode)

dim(ctd)
mik=cas1[cas1$DeviceCode=='MIK'&cas1$ActionCode=='DEBFIL',]
dim(mik)
carre=cas1[cas1$DeviceCode=='CARRE'&cas1$ActionCode=='DFIL',]
dim(carre)
wp2=cas1[cas1$DeviceCode=='WP2OF'&cas1$ActionCode=='VIR',]
dim(wp2)
mesopelo=cas1[cas1$DeviceCode=='MESOPELO'&cas1$ActionCode=='DFIL',]
dim(mesopelo)
dim(cufes)

# 1.6. WBTTUBE 
#*******************************************************
wbttube=cas1[cas1$DeviceCode=='WBTTUBE'&cas1$ActionCode=='SURF',]
length(unique(wbttube$OperationId))

# 2. Import des evts casino ------------------------------
# (pour completer les positions geographiques manquantes dans l'export tutti)
#*******************************************************
#2.1. a partir d'un export casino -----
#********************************
#copy casino evt file
#*******************
path.cas.echant3=paste(prefix,"\\",cruise,"\\","Casino",sep='')

shell(paste('cd ','"',paste(prefix,cruise,
                            '\\EvaluationBiomasse\\Donnees\\Casino\\',sep=''),
            '"','&& xcopy ','"',paste(path.cas.echant3,'casino_pelgas2018_evts.csv',sep=''),'" /e /y',sep=''))

path.cas=paste(path.cas.echant2,
               'PHOENIX2018_casino_evts_V3.csv',sep='')

casimp=casino.import(path.cas=path.cas,
                     stp.gears = c("76x70", "57x52","MIK",
                                   "MESOPELO","CARRE",
                                   "57x52ENROL"),
                     stp.events=c('DFIL','DPLON','DEBFIL',
                                  'FVIR','FINVIR'),
                     cruiseCode='',runlag=0,
                     dstart="03/06/2018 00:00:00",
                     check.strates=FALSE,sep=';',
                     start.events=c('DEBURAD','DEBUTRA','REPRAD','REPTRA'),
                     get.jackpot = FALSE)
names(casimp)
stp=casimp$stp
#extrait le fichier jackpot
jackpot=casimp$jackpot
#extrait les erreurs du fichier jackpot
errors=casimp$errors
#extrait les operations de peche
#head(jackpot)
head(stp)
operation.peche=stp[stp$EVT%in%c('DFIL','DEBFIL'),]
names(operation.peche)[names(operation.peche)=='NOSTA']='NumStation'
names(operation.peche)[names(operation.peche)=='LONF']='lon'
names(operation.peche)[names(operation.peche)=='LATF']='lat'
names(operation.peche)
# extrait le casino
casevt=casimp$cas
head(casevt)
#export du fichier jackpot
#write.table(jackpot,'T:/PELGAS2015/Casino/jackpototo.csv',sep=';',row.names=FALSE)

# Selection des bathysondes
#*************
head(casevt)
ctd=casevt[casevt$Code.Appareil=='BATHY'&casevt$Code.Action=='DEBFIL',]
#ctd=cas1[cas1$DeviceCode=='BATHY'&cas1$ActionCode=='DEBFIL',]
names(ctd)[c(3:5,9:16)]=c('TC','lat','lon','gearName','GEAR','nEVT','EVT',
                          'ID','COM','NumStation','STRATE')

# 2.2. ou directement a partir de la base casino du bord ----
#********************************
head(cas1)
operation.peche=cas1[cas1$ActionCode=='HQ'|(cas1$DeviceCode!='BATHY'&cas1$ActionCode=='DEBFIL'),]
names(operation.peche)[c(3:5,9:16)]=c('TC','lat','lon','gearName','GEAR','nEVT','EVT',
                           'ID','COM','NumStation','STRATE')
head(operation.peche)
# evts debut virage et filage
operation.peche2=cas1[cas1$ActionCode%in%c('DVIR','DFIL'),]
operation.peche2=operation.peche2[,c(3:5,12,15)]
names(operation.peche2)=c('TC','lat','lon','EVT','Num_Station')
operation.peche2$TC=format(strptime(operation.peche2$TC,
                                    "%Y-%m-%d %H:%M:%S",tz='GMT'),
                           "%d/%m/%Y %H:%M:%S")
operation.peche2$date=substr(operation.peche2$TC,1,10)
operation.peche2$heure=substr(operation.peche2$TC,12,19)
head(operation.peche2)
# evts cul a bord
operation.peche3=cas1[cas1$ActionCode%in%c('CULAB'),]
operation.peche3=operation.peche3[,c(3:5,12,15)]
names(operation.peche3)=c('TC','lat','lon','EVT','Num_Station')
head(operation.peche3)
operation.peche3$TC=format(strptime(operation.peche3$TC,
                                    "%Y-%m-%d %H:%M:%S",tz='GMT'),
       "%d/%m/%Y %H:%M:%S")
operation.peche3$date=substr(operation.peche3$TC,1,10)
operation.peche3$heure=substr(operation.peche3$TC,12,19)
operation.peche4=rbind(operation.peche2,operation.peche3)

coordChaluts.tutti=reshape(operation.peche4[,names(operation.peche2)!='TC'], 
            timevar = "EVT", idvar = c("Num_Station"),
            v.names=c('date','heure','lat','lon'),direction = "wide")
coordChaluts.tutti=coordChaluts.tutti[order(coordChaluts.tutti$Num_Station),]
coordChaluts.tutti$Num_Trait=seq(dim(coordChaluts.tutti)[1])
names(coordChaluts.tutti)=c('Num_Station','Date_Deb','Heure_Deb','Latdeb','Longdeb',
                            'Date_Fin','Heure_Fin','Latfin','Longfin',
                            'Date.CULAB','Heure.CULAB','Lat.CULAB','Long.CULAB',
                            'Num_Trait')
coordChaluts.tutti=coordChaluts.tutti[,c('Num_Station','Num_Trait','Date_Deb',
                                         'Heure_Deb','Latdeb','Longdeb','Date_Fin',
                                         'Heure_Fin','Latfin','Longfin','Date.CULAB',
                                         'Heure.CULAB','Lat.CULAB','Long.CULAB')]
head(coordChaluts.tutti)
# verifier les positions des chaluts 
plot(operation.peche$lon,operation.peche$lat,asp = 1/cos(46 * pi/180))
coast()
unique(operation.peche$NumStation)
length(unique(operation.peche$NumStation))

# export des infos chalut pour la salle de tri
write.table(coordChaluts.tutti,
            paste(path.cas.echant2,cruise,'_casino_infosChaluts.csv',sep=''),sep=';',
            row.names = FALSE)

# 3. Import donnees peche ------------------------------
#*******************************************************
# 3.1. Auto import et extraction de Tutti -----
#************
# Peche
#**************
prefix='M:\\'
prefix='G:\\'
# dossier ou se trouve l'export tutti
path.tuttiRaw=paste(prefix,cruise,"\\Data\\Peche\\exports_peche\\exports_generiques",sep='')
# dossier ou extraire l'export tutti
path.tuttiRaw2=paste(prefix,cruise,"\\Data\\Peche\\exports_peche\\exports_EchoR",sep='')
dir.create(file.path(path.tuttiRaw2), showWarnings = FALSE,recursive = TRUE)
# find latest tutti archive
lfiles=list.files(path.tuttiRaw)
lfiles2=file.info(paste(path.tuttiRaw,lfiles,sep='\\'))
sfile=lfiles2[lfiles2$mtime==max(lfiles2$mtime),]
nsfile=row.names(sfile)
# copy latest tutti file
shell(paste('copy ','"',nsfile,'" ','"',path.tuttiRaw2,'"',sep=''))
lfiles3=list.files(path.tuttiRaw2)
lfiles4=file.info(paste(path.tuttiRaw2,lfiles3,sep='\\'))
names(lfiles4)
ndl=lfiles4[!lfiles4$isdir,'mtime']
sfile2=lfiles4[lfiles4$mtime==max(ndl)&!lfiles4$isdir,]
nsfile2=row.names(sfile2)
# unzip latest tutti file
#shell(paste('7z x ','"',nsfile2,'"',' -aoa -o ','"',path.tuttiRaw2,'"',sep=''))
# To install archive package:
#devtools::install_github("jimhester/archive")
library(archive)
archive_extract(archive(nsfile2), path.tuttiRaw2)
# copy extracted files
lfiles5=list.files(path.tuttiRaw2)
lfiles6=file.info(paste(path.tuttiRaw2,lfiles5,sep='\\'))
sfile3=lfiles6[lfiles6$mtime==max(lfiles6$mtime),]
nsfile3=row.names(sfile3)
dir.create(file.path(paste(path.tuttiRaw2,'\\csv',sep='')), 
           showWarnings = FALSE,recursive = TRUE)
shell(paste('cd ','"',paste(path.tuttiRaw2,'\\csv',sep=''),'"','&& xcopy ','"',nsfile3,'" /e /y',sep=''))

# spCor=data.frame(spns=c('Scomber colias','Lithognathus mormyrus','Myliobatis aquila','Aurelia','Rhizostoma'),
#                  spcode=c('SCOM-JAP','LITH-MOR','MYLI-AQU','AURE-SPP','RHIZ-SPP'))

# 3.2. Import des donnees de peche ------
#************
path.tutti.peche=paste(prefix,cruise,'/Data/Peche/exports_peche/exports_EchoR/',sep='')
path.catch=paste(path.tutti.peche,'csv/catch.csv',sep='')
path.operations=paste(path.tutti.peche,'csv/operation.csv',sep='')
path.param=paste(path.tutti.peche,'csv/parameter.csv',sep='')
path.sampleCategory=paste(path.tutti.peche,'csv/sampleCategory.csv',sep='')

#lsp.fish=c("ENGR-ENC", "SARD-PIL", "SPRA-SPR", "TRAC-TRU", "TRAC-MED", 
#           "SCOM-SCO", "SCOM-COL", "MICR-POU","CAPR-APE","MERL-MCC")
#skipc=c("V0160","V0293")

peche.data=Tutti2EchoR(path.catch=path.catch,
                       path.operations=path.operations,
                       path.param=NULL,path.export=NULL,
                       spCor=NULL,
                       operation.peche=operation.peche,
                       path.sampleCategory = path.sampleCategory,
                       lsp = NULL,skipc=NULL,
                       check.strate = FALSE)

# extraction des donnees
names(peche.data)
TotalSamples4EchoR=peche.data$TotalSamples4EchoR
TotalSamples4EchoR$pro=substr(TotalSamples4EchoR$operationID,2,2)==5
lspALL=sort(unique(TotalSamples4EchoR$baracoudacode))
names(TotalSamples4EchoR)
TotalSamples4EchoR[TotalSamples4EchoR$baracoudacode=='',]
#head(TotalSamples4EchoR)

#names(TotalSamples4EchoR)
length(unique(TotalSamples4EchoR$operationID))
SubSamples4EchoR=peche.data$SubSamples4EchoR
length(unique(SubSamples4EchoR$operationID))
#names(SubSamples4EchoR)
metadata=peche.data$metadata
#metadata.pros=metadata[substr(metadata$Code_Station,2,2)==5,]
#metadata.pros$Engin="Chaluts boeufs pelagiques"
#head(metadata)
unique(metadata$STRATE)
operations=peche.data$OPERATION
#head(operations)
unique(operations$Strate_Id)
catch=peche.data$CATCH
param=peche.data$param
Peche.wide=peche.data$Peche.wide
param.wide=peche.data$param.wide
param.wide=param.wide[order(param.wide$Code_station),]
unique(param.wide$GEAR)

#head(param)
# chaluts manquants dans peche.wide
# sort(unique(param$Code_station))
# (msparam=unique(param$Code_station)[!unique(param$Code_station)%in%Peche.wide$NOSTA])
# (mscatch=unique(catch$Code_station)[!unique(catch$Code_station)%in%Peche.wide$NOSTA])
# param[param$Code_station%in%msparam,]
#operations[operations$Code_Station%in%msparam,]
table(operations$Engin)
#operations[operations$Code_Station%in%c('V5042','V5041'),]
unique(Peche.wide$GEAR)
unique(param.wide$GEAR)
head(operations)
#operations$pro=substr(operations$Code_Station,2,2)==5
#table(operations$pro)

# Peche.wide &co
dim(Peche.wide)
head(Peche.wide)
head(stp)
# add gear and depth in peche.wide
ustp=unique(stp[,c('NOSTA','STRATE','GEAR')])
ustp=stp[stp$EVT%in%c('DFIL','DEBFIL'),
         c('date','heure','NOSTA','STRATE','GEAR','SONDE')]
dim(ustp)
dim(Peche.wide)

Peche.wide=merge(Peche.wide[
  ,!names(Peche.wide)%in%c('GEAR','SONDE')],
                 ustp[,!names(ustp)%in%c('STRATE')],
  by.x='NOSTA',by.y='NOSTA')

#head(Peche.wide)
# Nb de chaluts
#table(Peche.wide$pro)
head(Peche.wide)
#Peche.wide$STRATE=substr(Peche.wide$STRATE,1,3)
#Peche.wide$pro=substr(Peche.wide$NOCHAL,2,2)==5
#dim(Peche.wide[!Peche.wide$pro,])
#param.wide=peche.data$param.wide
operations=peche.data$OPERATION
metadata=peche.data$metadata
#table(Peche.wide$pro)

# Check positions chaluts
#***********************
plot(Peche.wide$LONF,Peche.wide$LATF,pch=1,
     asp = 1/cos(46 * pi/180))
coast()

# 7.0 Export Fichiers pour SIG ------------------------
#**********************************
#export des fichiers peche pour SIG
write.table(Peche.wide,paste(
  prefix,cruise,'/Data/SIG/pelGIS/',cruise,'/Donnees/',cruise,
  '_PecheSIG.csv',sep=''),row.names=F,quote=F,sep=";")


# ******************************************************************
# 5.0 Verifications captures -------------------------------
#*******************************************************
# NA dans longueurs et poids moyens
#******************
names(TotalSamples4EchoR)
# nb de chaluts Thalassa :
length(unique(TotalSamples4EchoR[substr(TotalSamples4EchoR$operationID,2,2)!=5,'operationID']))

lsp=c('ENGR-ENC','SARD-PIL','TRAC-TRU','TRAC-MED','SCOM-SCO','SCOM-COL','MICR-POU','CAPR-APE','MERL-MCC')
# Longueurs moyennes manquantes pour les especes de lsp
(tssNA=TotalSamples4EchoR[is.na(TotalSamples4EchoR$meanLength)&TotalSamples4EchoR$baracoudacode%in%lsp,])

# check taille-poids moyens eleves au chalut
#******************
library(lattice)
par(mfrow=c(1,1))
#lsp=c('ENGR-ENC','SARD-PIL','TRAC-TRU','TRAC-MED','SCOM-SCO','SCOM-JAP','MICR-POU','SPRA-SPR')
# relations taille-poids eleves au chalut
LW.compute(catch=TotalSamples4EchoR,lspcodes.mens=lsp,codespname='baracoudacode',
           Lname='meanLength',Wname='meanWeight',Wscaling=1)

TotalSamples4EchoR[TotalSamples4EchoR$operationID=='W5002',]

# pour afficher les poids moyens douteux :
# on selectionne les poissons douteux en cliquant sur la figure
# nom de l espece a verifier :
spcheck='CAPR-APE'
ssANE=TotalSamples4EchoR[TotalSamples4EchoR$baracoudacode==spcheck,]
head(ssANE)
summary(ssANE$PM)
par(mfrow=c(1,1))
plot(ssANE$meanLength,ssANE$meanWeight)
p=identify(ssANE$meanLength,ssANE$meanWeight)
(ssANEs=ssANE[p,])
plot(ssANEs$meanLength,ssANEs$meanWeight)

# distributions poids et longueurs moyennes pro et pas pro
bwplot(meanWeight~baracoudacode|pro,
       TotalSamples4EchoR[TotalSamples4EchoR$baracoudacode%in%lsp,],main='Chalut pro?')
bwplot(meanLength~baracoudacode|pro,
       TotalSamples4EchoR[TotalSamples4EchoR$baracoudacode%in%lsp,],main='Chalut pro?')
names(TotalSamples4EchoR)
TotalSamplesPro=TotalSamples4EchoR[TotalSamples4EchoR$pro,]
TotalSamplesThala=TotalSamples4EchoR[!TotalSamples4EchoR$pro,]
# stats descriptives poids moyens chaluts pros et pas pros
aggregate(TotalSamplesPro$meanWeight,list(TotalSamplesPro$baracoudacode),summary)
aggregate(TotalSamplesThala$meanWeight,list(TotalSamplesThala$baracoudacode),summary)
# relations taille-poids eleves au chalut pro/thalassa
LW.compute(catch=TotalSamplesPro,lspcodes.mens=lsp,codespname='baracoudacode',
           Lname='meanLength',Wname='meanWeight',Wscaling=1,plabs='operationID')
LW.compute(catch=TotalSamplesThala,lspcodes.mens=lsp,codespname='baracoudacode',
           Lname='meanLength',Wname='meanWeight',Wscaling=1,plabs='operationID')

# check taille-poids mensurations
#******************
SubSamples4EchoR2=SubSamples4EchoR
SubSamples4EchoR2$PM=as.numeric(SubSamples4EchoR2$weightAtLength)/SubSamples4EchoR2$numberAtLength
summary(SubSamples4EchoR2[SubSamples4EchoR2$baracoudacode=='ENGR-ENC','PM'])
summary(SubSamples4EchoR2[SubSamples4EchoR2$baracoudacode=='SARD-PIL','PM'])
head(SubSamples4EchoR2)
summary(SubSamples4EchoR2[SubSamples4EchoR2$baracoudacode=='ENGR-ENC','lengthClass'])

# Relations taille-poids mensurations
names(SubSamples4EchoR2)
LW.mens=LW.compute(catch=SubSamples4EchoR2,codespname='baracoudacode',
                   Lname='lengthClass',Wname='PM',wname='NBIND',plotit=TRUE,Ncol=4)

sspros=SubSamples4EchoR2[substr(SubSamples4EchoR2$operationID,2,2)==5,]
# pour afficher les poids moyens douteux :
# pour l anchois
ssANE=SubSamples4EchoR2[SubSamples4EchoR2$baracoudacode=='TRAC-TRU',]
head(ssANE)
summary(ssANE$PM)
# selection dans plages tailles et poids moyens
anepb1=ssANE[!is.na(ssANE$PM)&ssANE$PM>=.060,]
anepb2=ssANE[!is.na(ssANE$PM)&ssANE$PM>0.03&ssANE$lengthClass<=15,]
anepb3=ssANE[!is.na(ssANE$PM)&ssANE$PM>0.015&ssANE$lengthClass<=12,]
anepbtot=rbind(anepb1,anepb2)
par(mfrow=c(1,1))
plot(ssANE$lengthClass,ssANE$PM)
# ou selectionne poissons douteux en cliquant sur le graphique
p=identify(ssANE$lengthClass,ssANE$PM,
           labels=paste(ssANE$operationID,ssANE$lengthClass,ssANE$PM))
ssANE[p,]
plot(ssANEs$lengthClass,ssANEs$PM)

# pour la sardine 
ssPIL=SubSamples4EchoR2[SubSamples4EchoR2$baracoudacode=='SARD-PIL',]
head(ssPIL)
pilpb2=ssPIL[!is.na(ssPIL$PM)&ssPIL$PM>0.07&ssPIL$lengthClass<17,]
SubSamples4EchoR2[SubSamples4EchoR2$operationID=='T5038',]
# pour le sprat
ssSPR=SubSamples4EchoR2[SubSamples4EchoR2$baracoudacode=='SPRA-SPR',]
head(ssSPR)
pilpb2=ssSPR[!is.na(ssSPR$PM)&ssSPR$PM>0.010&ssSPR$lengthClass<10,]
SubSamples4EchoR2[SubSamples4EchoR2$operationID=='T5038',]
# pour le merlan bleu
ssWHB=SubSamples4EchoR2[SubSamples4EchoR2$baracoudacode=='MICR-POU',]
head(ssWHB)
pilpb2=ssWHB[!is.na(ssWHB$PM)&ssWHB$PM<0.120&ssWHB$lengthClass>28,]
SubSamples4EchoR2[SubSamples4EchoR2$operationID=='T5038',]

#*********************************
# Remove juvenile sprat
#*********************************
# dim(TotalSamples4EchoR)
# TotalSamples4EchoRrem=TotalSamples4EchoR[(TotalSamples4EchoR$baracoudacode=='SPRA-SPR'&
#                                            !is.na(TotalSamples4EchoR$meanLength)&
#                                            TotalSamples4EchoR$meanLength<60),]
# TotalSamples4EchoRs=TotalSamples4EchoR[-as.numeric(row.names(TotalSamples4EchoRrem)),]
# dim(TotalSamples4EchoRs)
# 
# dim(SubSamples4EchoR)
# SubSamples4EchoRrem=SubSamples4EchoR[(SubSamples4EchoR$baracoudacode=='SPRA-SPR'&
#                                            !is.na(SubSamples4EchoR$lengthClass)&
#                                            SubSamples4EchoR$lengthClass<6),]
# SubSamples4EchoRs=SubSamples4EchoR[-as.numeric(row.names(SubSamples4EchoRrem)),]
# dim(SubSamples4EchoRs)

#***********************************************************************
# 6.0 export des captures et mensurations pour evaluation biomasse -------------------
#***********************************************************************
head(TotalSamples4EchoR)
head(metadata.pros)
plot(metadata.pros$LongDeb,metadata.pros$LatDeb)
plot(metadata.pros$LongFin,metadata.pros$LatFin)

dir.create(file.path(paste(prefix,cruise,'/EvaluationBiomasse/Donnees/Peche/',sep='')), 
                     showWarnings = FALSE,recursive = TRUE)

write.table(TotalSamples4EchoR,paste(prefix,cruise,'/EvaluationBiomasse/Donnees/Peche/',
                                     'TotalSamples4EchoR.csv',sep=''),
            row.names=F,quote=F,sep=";")

write.table(SubSamples4EchoR,paste(prefix,cruise,'/EvaluationBiomasse/Donnees/Peche/',
                                   'SubSamples4EchoR.csv',sep=''),row.names=F,quote=F,sep=";")

write.table(metadata.pros,paste(prefix,cruise,'/EvaluationBiomasse/Donnees/Peche/','metadata.pros.csv',
                                sep=''),row.names=F,quote=F,sep='\t')



#***********************************************************************
# Optionnel, si besoin pour verifier: import de l export Sumatra ---------------------------------------------
#******************
path.sumatra='T:/PELGAS2015/Peche/tutti/exports tutti vers sumatra/sumatra_PELGAS_2015_17-05-2015_14h16TU.csv'
isumatra=read.csv2(path.sumatra)
head(isumatra)
isumatra$longueurmoy=as.numeric(as.character(isumatra$longueurmoy))
isumatra$poidsmoy=as.numeric(as.character(isumatra$poidsmoy))
isumatra$signe
isumatra$pmi=isumatra$poidsmoy*isumatra$nbindividus
  
misumatra=aggregate(isumatra[,c('longueurmoy','poidsmoy')],list(isumatra$espececampagne),mean)

sisumatra=aggregate(isumatra[,c('nbindividus')],list(espececampagne=isumatra$espececampagne),sum)
names(sisumatra)[2]="nbtot"

isumatra2=merge(isumatra,sisumatra,by.x='espececampagne',by.y='espececampagne')
isumatra2$pmi=isumatra2$poidsmoy*isumatra2$nbindividus/isumatra2$nbtot
isumatra2$lmi=isumatra2$longueurmoy*isumatra2$nbindividus/isumatra2$nbtot
wmisumatra=aggregate(isumatra2[,c('lmi','pmi')],list(isumatra2$espececampagne),sum)
names(wmisumatra)=c('sp','wLM','wPM')

lsp=c('ENGR-ENC','SARD-PIL','TRAC-TRU','TRAC-MED','SCOM-SCO','SCOM-JAP','MICR-POU')

LW.compute(catch=isumatra,codespname='espececampagne',lspcodes.mens=lsp,
           Lname='longueurmoy',Wname='poidsmoy',Wscaling=1,compute.LW=FALSE)

isumatras=isumatra[isumatra$espececampagne=='SARD-PIL',]
plot(isumatras$longueurmoy,isumatras$poidsmoy)



# 7.0 Georeferencement des photos d'ecran ----------------------------------------------
#******************************************
head(casevt)
pecran=casevt[casevt$Code.Action=='PECRAN',c('Date','Heure','Latitude','Longitude','Code.Action',
                                       'N..Station','Strate')]
photos=pecran[pecran$Strate!='video',]
videos=pecran[pecran$Strate=='video',]

# 7.1. images catalogue chalutage ---------
#******************************************
path.catalogue=paste(prefix,cruise,'\\Acoustique\\Catalogue\\',sep='')
lf1.catalogue=list.files(path.catalogue,pattern='*.jpg')
lf2.catalogue=list.files(path.catalogue,pattern='*.png')
lf3.catalogue=list.files(path.catalogue,pattern='*.PNG')
#lf4.catalogue=list.files(path.catalogue,pattern='*.JPG')
lf.catalogue=c(lf1.catalogue,lf2.catalogue,lf3.catalogue)
N=length(lf.catalogue)
lfs.catalogue=data.frame(NOSTA=substr(lf.catalogue,1,5),
                         t(data.frame(strsplit(substr(lf.catalogue,5,50),split='[.]'))))
row.names(lfs.catalogue)=seq(length(lfs.catalogue[,1]))
N=dim(lfs.catalogue)[2]
names(lfs.catalogue)=c('NOSTA','desc','ext')
lfs.catalogue$cat='peche'
lfs.catalogue[grepl('ref',lfs.catalogue$desc),'cat']='ref'
lfs.catalogue[grepl('marport',lfs.catalogue$desc),'cat']='marport'
lfs.catalogue$path=paste(path.catalogue,lf.catalogue,sep='')
lfs.catalogue$path2=lf.catalogue
lfs.catalogue=merge(lfs.catalogue,stp[,c('NOSTA','date','heure','LATF','LONF','STRATEL')],
                    by.x='NOSTA',by.y='NOSTA')
lfs.catalogues=lfs.catalogue[lfs.catalogue$cat=='ref',]

#export du fichier des photos catalogue
write.table(lfs.catalogues,paste(path.pelGIS,cruise,'_pelGIScatalogueChaluts.csv',sep=''),sep=',',row.names=FALSE)

# 7.2. photos ecran -------------
#******************************************
path.Ncatalogue=paste(prefix,cruise,'/Acoustique/PhotosEcran/',sep='')
lf1.Ncatalogue=list.files(path.Ncatalogue,pattern='*.jpg')
lf2.Ncatalogue=list.files(path.Ncatalogue,pattern='*.png')
lf3.Ncatalogue=list.files(path.Ncatalogue,pattern='*.PNG')
lf4.Ncatalogue=list.files(path.Ncatalogue,pattern='*.JPG')
lf.Ncatalogue=sort(c(lf1.Ncatalogue,lf2.Ncatalogue,lf3.Ncatalogue,lf4.Ncatalogue))
N=length(lf.Ncatalogue)
#♪lfs.Ncatalogue=data.frame(t(data.frame(strsplit(lf.Ncatalogue,split='_'))))
lfs.Ncatalogue=data.frame(NOSTA=substr(lf.Ncatalogue,1,8),
                          desc=substr(lf.Ncatalogue,9,50))
row.names(lfs.Ncatalogue)=seq(length(lfs.Ncatalogue[,1]))
N=dim(lfs.Ncatalogue)[2]
lfs.Ncatalogue$numphoto=as.numeric(substr(lfs.Ncatalogue$NOSTA,6,8))
lfs.Ncatalogue$path=paste(path.Ncatalogue,lf.Ncatalogue,sep='')
lfs.Ncatalogue$pathRel=lf.Ncatalogue
# export du casino photos
write.table(photos,paste(path.cas.echant2,cruise,'_CasinoPhotosEcran.csv',sep=''),
            sep=',',row.names=FALSE)
photos$N..Station=as.numeric(as.character(photos$N..Station))
lfs.Ncatalogue=merge(lfs.Ncatalogue,photos,by.x='numphoto',by.y='N..Station')

#export du fichier des photos non catalogue
write.table(lfs.Ncatalogue,paste(path.pelGIS,cruise,'_pelGISphotosEcran.csv',sep=''),
            sep=',',row.names=FALSE)

# 7.3. videos --------------
#******************************************
lf.video=list.files(path.Ncatalogue,pattern='*.avi')
N=length(lf.video)
lfs.video=data.frame(NOSTA=substr(lf.video,1,8),
                          desc=substr(lf.video,9,50))
N=dim(lfs.video)[2]
lfs.video$numVideo=as.numeric(substr(lfs.video$NOSTA,6,9))
lfs.video$path=paste(path.Ncatalogue,lf.video,sep='')
videos$N..Station=as.numeric(as.character(videos$N..Station))
# export du casino photos
write.table(videos,paste(path.cas.echant2,cruise,'_CasinoVideos.csv',sep=''),
            sep=',',row.names=FALSE)
#export du fichier des videos
write.table(lfs.video,paste(path.pelGIS,'pelGISvideos.csv',sep=''),sep=',',row.names=FALSE)
lfs.video=merge(lfs.video,videos,by.x='numVideo',by.y='N..Station')



# photos ecran EK80
#******************************************
path.ek80='T:/PELGAS2016/EK80/photos_ecran/RUNs/'
lf.ek80=list.files(path.ek80,pattern='*.jpg',recursive = TRUE)
N=length(lf.ek80)
lfs.ek80=data.frame(t(data.frame(strsplit(lf.ek80,split='_'))))
row.names(lfs.ek80)=seq(length(lfs.ek80[,1]))
N=dim(lfs.ek80)[2]
names(lfs.ek80)=c('sdate','stime','type')
#head(lfs.ek80)
lfs.ek80$t=strptime(paste(substr(lfs.ek80$sdate,8,15),substr(lfs.ek80$stime,2,7)),format='%Y%m%d %H%M%S',tz='GMT')
lfs.ek80$tr=format(round(lfs.ek80$t,'mins'))
lfs.ek80$path=paste(path.ek80,lf.ek80,sep='')
N=dim(cas2)[1]
cas2s2=cas2[seq(1,N,2),]
#head(cas2s2)
cas2s2$tr=format(round(cas2s2$t,'mins'))
dim(lfs.ek80)
lfs.ek80=merge(lfs.ek80,unique(cas2s2[,c('tr','Longitude','Latitude')]),by.x='tr',by.y='tr')
dim(lfs.ek80)
#export des photos ecran ek80
write.table(lfs.ek80,paste(path.pelGIS,'EK80images.csv',sep=''),sep=',',row.names=FALSE)
  
