library(EchoR)
library(FactoMineR)
library(scatterpie)
library(ggplot2)

prefix='G:/'
pref='/media/mathieu/IfremerMData/'
cruise='PHOENIX2018'

path.export.nekton=paste(prefix,cruise,'/Resultats/necton/',sep='')

Peche.wide=read.table(paste(
  prefix,cruise,'/Data/SIG/pelGIS/',cruise,'/Donnees/',cruise,
  '_PecheSIG.csv',sep=''),header=TRUE,sep=";")
head(Peche.wide)
row.names(Peche.wide)=Peche.wide$NOSTA
Peche.wide$heure
Peche.wide$diel=c('D','D','D','N','N','N','D','D','D','N','N','N','D','D')

# PCA on micronekton catches ----
Peche.wide.mn=Peche.wide[Peche.wide$GEAR!="57x52ENROL",]
Peche.wide.mn$maxDepth=as.numeric(gsub('m','',Peche.wide.mn$STRATE))
names(Peche.wide.mn)

# Micronekton catches piecharts on a map: ----
ggplot()+geom_scatterpie(aes(x=LONF, y=LATF), 
                         data = Peche.wide.mn, 
                         cols=names(Peche.wide.mn)[3:29])

Peche.wide.mns=Peche.wide.mn[,c(3:29,39,36,38)]
Peche.wide.mns=Peche.wide.mns[,apply(Peche.wide.mns,2,sum)>0]
names(Peche.wide.mns)

micronekton.phoenix1.pca = PCA(Peche.wide.mns, scale.unit=TRUE, 
                           ncp=12, graph=T,quali.sup=29:30,quanti.sup = 28)

plot.PCA(micronekton.phoenix1.pca, axes=c(1, 2), choix="ind")
plot.PCA(micronekton.phoenix1.pca, axes=c(2, 3), choix="ind")
plot.PCA(micronekton.phoenix1.pca, axes=c(1, 2), choix="var")
plot.PCA(micronekton.phoenix1.pca, axes=c(1, 2), choix="varcor")

plotellipses(micronekton.phoenix1.pca)

micronekton.phoenix1.pca.dimdesc=dimdesc(micronekton.phoenix1.pca, axes=c(1,2))

# Clustering on PCs ----------

micronekton.phoenix1.pca.hcpc = HCPC(micronekton.phoenix1.pca)
names(micronekton.phoenix1.pca.hcpc)
micronekton.phoenix1.pca.hcpc$desc.var
head(Peche.wide.mn)
Peche.wide.mn.clust=merge(Peche.wide.mn,
                           micronekton.phoenix1.pca.hcpc$data.clust[
                             ,c('PT_ZEUS_FAB','clust')],
                                  by.x='NOSTA',by.y='row.names')
head(Peche.wide.mn.clust)

plot(Peche.wide.mn.clust$LONF,Peche.wide.mn.clust$LAT,
     col=Peche.wide.mn.clust$clust,pch=as.numeric(as.character(
       Peche.wide.mn.clust$clust)))

head(TotalSamples)
TotalSamples[TotalSamples$operationID=='W0356',]

save(list=c('Peche.wide.mns','micronekton.phoenix1.pca','Peche.wide.mn.clust'),
     file=paste(path.export.nekton,'PHOENIXnekton.RData',sep=''))
     
