library(EchoR)
pref='G:/'
pref='/media/mathieu/IfremerMData/'

path.nav=paste(pref,
  'EchoSonde/CampagnesEchoSonde/Metadata/NavigationEchosonde/2017-2021/',sep='')

lnavfiles=list.files(path.nav,pattern = '*.NA')
lnavfiles.df=data.frame(lnavfiles)
lnavfiles.df$survey=c('EchoSonde2017-1','EchoSonde2017-2','EchoSonde2018',
                      'EchoSonde2019-1','EchoSonde2019-2')

nav.df=NULL

for (i in 1:length(lnavfiles)){
  lnavi=read.table(paste(path.nav,lnavfiles[i],sep=''),sep=',')
  names(lnavi)=c('type','date','heure','type2','latLet','latInt','latDec',
                 'lonLet','lonInt','lonDec','letter','num','datum','unknown1',
                 'unknown2','unknown3','unknown4','unknown5','unknown6',
                 'unknown7')
  lnavi$survey=lnavfiles.df$survey[i]
  nav.df=rbind(nav.df,lnavi)
}
head(nav.df)
nav.df$latitude=nav.df$latInt+(floor(nav.df$latDec)*100/60+nav.df$latDec-floor(nav.df$latDec))/100
nav.df$longitude=-nav.df$lonInt+(floor(nav.df$lonDec)*100/60+nav.df$lonDec-floor(nav.df$lonDec))/100
range(nav.df$latitude)
range(nav.df$longitude)

plot(nav.df$longitude,nav.df$latitude)
coast()
