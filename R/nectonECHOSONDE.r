# Micronecton campagnes EchoSonde

pref='D:/'
path.mn.echo=paste(pref,'OwnCloud/documents/EchoSonde/Data/echosonde_WP2_Carré_méso.csv',sep='')

mn.echo=read.csv(path.mn.echo,sep=';',header=TRUE)
head(mn.echo)
names(mn.echo)=c('date','net','numStation','taxon','nb','taille_cm','groupsp','stage')
mn.echo$mois=substr(mn.echo$date,1,2)
mn.echo$annee=substr(mn.echo$date,4,7)
mn.echo$nb.rel=mn.echo$nb
mn.echo$groupsp[mn.echo$groupsp=="Salpida "]="Salpida"
mn.echo$groupsp[mn.echo$groupsp=="Chondrichthyes "]="Chondrichthyes"

mn.echo.nqual=mn.echo[mn.echo$nb%in%c('+','++','+++','++++'),]
mn.echo.nqual$nb.rel[mn.echo.nqual$nb.rel=='+']=.25
mn.echo.nqual$nb.rel[mn.echo.nqual$nb.rel=='++']=.50
mn.echo.nqual$nb.rel[mn.echo.nqual$nb.rel=='+++']=.75
mn.echo.nqual$nb.rel[mn.echo.nqual$nb.rel=='++++']=1

mn.echo.nquant=mn.echo[!mn.echo$nb%in%c('+','++','+++','++++',''),]
mn.echo.nmax=aggregate(mn.echo.nquant[,c('nb')],list(groupsp=mn.echo.nquant$groupsp),max)
names(mn.echo.nmax)[2]='Nmax'
mn.echo.nquant=merge(mn.echo.nquant,mn.echo.nmax,by.x='groupsp',by.y='groupsp')
head(mn.echo.nquant)
mn.echo.nquant$nb=as.numeric(mn.echo.nquant$nb)
mn.echo.nquant$Nmax=as.numeric(mn.echo.nquant$Nmax)
mn.echo.nquant$nb.rel=mn.echo.nquant$nb/mn.echo.nquant$Nmax

mn.echo2=plyr::rbind.fill(mn.echo.nqual,mn.echo.nquant)

mn.echo2$nb.rel=as.numeric(mn.echo2$nb.rel)

mn.echoa=aggregate(
  mn.echo2[,c('nb.rel','taille_cm')],
  list(net=mn.echo2$net,mois=mn.echo2$mois,annee=mn.echo2$annee,
       groupsp=mn.echo2$groupsp),
  mean,na.rm=TRUE)

mn.echoa.mois=aggregate(
  mn.echo2[,c('nb.rel','taille_cm')],
  list(net=mn.echo2$net,mois=mn.echo2$mois,groupsp=mn.echo2$groupsp),
  mean,na.rm=TRUE)

library(ggplot2)

ggplot(mn.echoa[mn.echoa$net=='mésopel'&
                  !mn.echoa$groupsp%in%c("Chondrichthyes ","Algae",
                                         "Syngnathidae"),],
       aes(x=mois,y=nb.rel,fill=factor(groupsp)))+
  geom_bar(stat="identity")+facet_wrap(vars(annee),ncol=1)

ggplot(mn.echoa.mois[mn.echoa.mois$net=='mésopel'&
                  !mn.echoa.mois$groupsp%in%c("Chondrichthyes ","Algae",
                                         "Syngnathidae"),],
       aes(x=mois,y=nb.rel,fill=factor(groupsp)))+
  geom_bar(stat="identity")

ggplot(mn.echoa.mois[mn.echoa.mois$net=='mésopel'&
                       !mn.echoa.mois$groupsp%in%c("Chondrichthyes ","Algae",
                                                   "Syngnathidae"),],
       aes(x=mois,y=nb.rel,group=factor(groupsp),col=factor(groupsp)))+
  geom_line(size=1.5)+ggtitle('Mesopelagos')

ggplot(mn.echoa.mois[mn.echoa.mois$net=='Carré'&
                       !mn.echoa.mois$groupsp%in%c("Chondrichthyes ","Algae",
                                                   "Syngnathidae"),],
       aes(x=mois,y=nb.rel,group=factor(groupsp),col=factor(groupsp)))+
  geom_line(size=1.5)+ggtitle('Carré')


unique(mn.echoa.mois$groupsp)

# Add PHOENIX2018
#*************************
head(Peche.wide.mn)
names(Peche.wide.mn)

Peche.long.mn=reshape(
  Peche.wide.mn,idvar = c("NOSTA","CAMPAGNE","LONF","LATF","STRATE","date",
                          "heure","GEAR","SONDE","diel","maxDepth","TOT"), 
  varying = list(3:29),v.names = "PT", direction = "long",timevar = "sp",
  times=names(Peche.wide.mn)[3:29])
row.names(Peche.long.mn)=seq(dim(Peche.long.mn)[1])
head(Peche.long.mn)

Peche.long.mn$sp=substr(Peche.long.mn$sp,4,11)

groupsp.df=data.frame(sp=unique(Peche.long.mn$sp),
                      groupsp=c("Cnidaria","Chondrichthyes","Cnidaria",
                                "Chondrichthyes","Ctenophora","Cnidaria",
                                "Chondrichthyes","Chondrichthyes",
                                "Chondrichthyes","Chondrichthyes",
                                "Chondrichthyes","Crustacea","Cnidaria",
                                "Chondrichthyes","Chondrichthyes","Amphipoda",
                                "Ctenophora",
                                "Crustacea","Chondrichthyes","Chondrichthyes",
                                "Chondrichthyes","Cephalopoda","Chondrichthyes",
                                "Chondrichthyes","Syngnathidae",
                                "Chondrichthyes","Chondrichthyes"))

Peche.long.mn=merge(Peche.long.mn,groupsp.df,by.x='sp',by.y='sp')
head(Peche.long.mn)

groupsp.df$sp=gsub('_','-',groupsp.df$sp)

Peche.long.mna=aggregate(Peche.long.mn$PT,list(Peche.long.mn$groupsp,))

Peche.long.mna=aggregate(Peche.long.mn$PT,list(Peche.long.mn$groupsp,))

TotalSamples4EchoR.phoenix2018=read.table(paste(
  prefix,'Campagnes/PHOENIX2018/Data/Necton/','TotalSamples4EchoR.csv',sep=''),
  sep=';',header=TRUE)
head(TotalSamples4EchoR.phoenix2018)

TotalSamples4EchoR.phoenix2018=merge(TotalSamples4EchoR.phoenix2018,groupsp.df,
                                     by.x='baracoudacode','sp')
head(Peche.wide.mn)
TotalSamples4EchoR.phoenix2018=merge(
  TotalSamples4EchoR.phoenix2018,
  unique(Peche.wide.mn[,c('NOSTA','date','GEAR','diel')]),
                                     by.x='operationID',by.y='NOSTA')
head(TotalSamples4EchoR.phoenix2018)

TotalSamples4EchoR.phoenix2018[
  TotalSamples4EchoR.phoenix2018$groupsp!="Chondrichthyes",]

mn.phoenix2018=TotalSamples4EchoR.phoenix2018[
  ,c('operationID','date','baracoudacode','numberSampled','meanLength',
     'groupsp','GEAR')]
names(mn.phoenix2018)=c('numStation','date','taxon','nb','taille_cm','groupsp','net')
mn.phoenix2018$mois=substr(mn.phoenix2018$date,4,5)
mn.phoenix2018$annee=substr(mn.phoenix2018$date,7,11)

# Complete dataset
head(mn.echo.nquant)

mn.echo.nquant.tot=rbind(mn.phoenix2018,mn.echo.nquant[,names(mn.phoenix2018)])
unique(mn.echo.nquant.tot$net)
mn.echo.nquant.tot$net[mn.echo.nquant.tot$net=='Carré']='CARRE'
mn.echo.nquant.tot$net[mn.echo.nquant.tot$net=='mésopel']='MESOPELO'

mn.echo.nquant.tot.nmax=aggregate(mn.echo.nquant.tot[,c('nb')],
                           list(groupsp=mn.echo.nquant.tot$groupsp,
                                net=mn.echo.nquant.tot$net),max)
names(mn.echo.nquant.tot.nmax)[3]='Nmax'
mn.echo.nquant.tot=merge(mn.echo.nquant.tot,mn.echo.nquant.tot.nmax,
                  by.x=c('groupsp','net'),by.y=c('groupsp','net'))
head(mn.echo.nquant.tot)
mn.echo.nquant.tot$nb=as.numeric(mn.echo.nquant.tot$nb)
mn.echo.nquant.tot$Nmax=as.numeric(mn.echo.nquant.tot$Nmax)
mn.echo.nquant.tot$nb.rel=mn.echo.nquant.tot$nb/mn.echo.nquant.tot$Nmax

mn.echo2.tot=plyr::rbind.fill(mn.echo.nqual,mn.echo.nquant.tot)
head(mn.echo2.tot)
mn.echo2.tot$net[mn.echo2.tot$net=='Carré']='CARRE'
mn.echo2.tot$net[mn.echo2.tot$net=='mésopel']='MESOPELO'

mn.echo2.tot$nb.rel=as.numeric(mn.echo2.tot$nb.rel)

mn.echoa2=aggregate(
  mn.echo2.tot[,c('nb.rel','taille_cm')],
  list(net=mn.echo2.tot$net,mois=mn.echo2.tot$mois,annee=mn.echo2.tot$annee,
       groupsp=mn.echo2.tot$groupsp),
  mean,na.rm=TRUE)

mn.echoa2.mois=aggregate(
  mn.echo2.tot[,c('nb.rel','taille_cm')],
  list(net=mn.echo2.tot$net,mois=mn.echo2.tot$mois,
       groupsp=mn.echo2.tot$groupsp),
  mean,na.rm=TRUE)

mn.echoa2.tota=aggregate(
  mn.echo2.tot[,c('nb.rel','taille_cm')],
  list(net=mn.echo2.tot$net,groupsp=mn.echo2.tot$groupsp),
  mean,na.rm=TRUE)

mn.echoa2.tota.sp.station=aggregate(
  mn.echo2.tot[,c('nb.rel')],
  list(net=mn.echo2.tot$net,groupsp=mn.echo2.tot$groupsp,
       station=mn.echo2.tot$numStation,
       mois=mn.echo2.tot$mois),
  sum,na.rm=TRUE)

table(mn.echo2.tot$groupsp,mn.echo2.tot$net)

mn.echo2.tot.meso=mn.echo2.tot[mn.echo2.tot$net=='MESOPELO',]

mn.echoa2.tota.sp.station.meso=mn.echoa2.tota.sp.station[
  mn.echoa2.tota.sp.station$net=='MESOPELO',]

mn.echoa2.tota.sp.station.mesos=mn.echoa2.tota.sp.station.meso[
  mn.echoa2.tota.sp.station.meso$groupsp%in%c("Ctenophora","Cnidaria",
                                              "Salpida","Syngnathidae"),]

library(corrplot)
corrplot(table(mn.echo2.tot.meso$groupsp,mn.echo2.tot.meso$mois),
         is.corr = FALSE,col=viridis(10))

corrplot(table(mn.echoa2.tota.sp.station.meso$groupsp,
               mn.echoa2.tota.sp.station.meso$mois),
         is.corr = FALSE,col=viridis(10))

corrplot(table(mn.echoa2.tota.sp.station.mesos$groupsp,
               mn.echoa2.tota.sp.station.mesos$mois),
         is.corr = FALSE,col=viridis(10))


ggplot(mn.echoa2.tota[mn.echoa2.tota$net=='MESOPELO',],
       aes(x=groupsp,y=nb.rel))+geom_bar(stat="identity")

library(ggplot2)

ggplot(mn.echoa2[mn.echoa2$net=='MESOPELO'&
                  mn.echoa2$groupsp%in%c("Cnidaria","Ctenophora",
                                         "Salpida"),],
       aes(x=mois,y=nb.rel,fill=factor(groupsp)))+
  geom_bar(stat="identity")+facet_wrap(vars(annee),ncol=1)

ggplot(mn.echoa2.mois[mn.echoa2.mois$net=='MESOPELO'&
                       mn.echoa2.mois$groupsp%in%c("Cnidaria","Ctenophora",
                                                   "Salpida"),],
       aes(x=mois,y=nb.rel,fill=factor(groupsp)))+
  geom_bar(stat="identity")

ggplot(mn.echoa2.mois[mn.echoa2.mois$net=='MESOPELO'&
                       mn.echoa2.mois$groupsp%in%c("Cnidaria","Ctenophora",
                                                   "Salpida"),],
       aes(x=mois,y=nb.rel,group=factor(groupsp),col=factor(groupsp)))+
  geom_line(size=1.5)+ggtitle('Mesopelagos')

ggplot(mn.echoa2.mois[mn.echoa2.mois$net=='CARRE'&
                       mn.echoa2.mois$groupsp%in%c("Cnidaria","Ctenophora",
                      "Salpida"),],
       aes(x=mois,y=nb.rel,group=factor(groupsp),col=factor(groupsp)))+
  geom_line(size=1.5)+ggtitle('Carré')
