library(EchoR)
library(ggplot2)
library(sf)
library(maptools)
library(marmap)
library(scatterpie)
library(rnaturalearth)
library(FactoMineR)

# Import abundance per station and taxon ----
path.ecotaxa.phoenix1='/media/mathieu/IfremerMData/PHOENIX2018/Data/ZooPlk/export_summary_4029_20210921_1643.tsv'

ecotaxa.phoenix1=read.csv(path.ecotaxa.phoenix1,header=TRUE,sep='\t')
head(ecotaxa.phoenix1)
names(ecotaxa.phoenix1)[names(ecotaxa.phoenix1)=='nbr']='value'
ecotaxa.phoenix1$display_name

# WP2 map ----
ecotaxa.phoenix1.wp2s=unique(ecotaxa.phoenix1[
  ,c('orig_id','latitude','longitude','date')])
dim(ecotaxa.phoenix1.wp2s)
plot(ecotaxa.phoenix1.wp2s$longitude,ecotaxa.phoenix1.wp2s$latitude)
coast()

# Summarise dataset with PCA -----
head(ecotaxa.phoenix1)
length(unique(ecotaxa.phoenix1$display_name))
ecotaxa.phoenix1.wide=reshape(ecotaxa.phoenix1,v.names = "value", 
                              idvar = c("orig_id","latitude","longitude","date"),
                              timevar = "display_name", direction = "wide")
head(ecotaxa.phoenix1.wide)
names(ecotaxa.phoenix1.wide)[-seq(4)]=gsub('value.','',
                                           names(ecotaxa.phoenix1.wide)[-seq(4)])
ecotaxa.phoenix1.wide[,-seq(4)]=apply(ecotaxa.phoenix1.wide[,-seq(4)],2,na.null)
ecotaxa.phoenix1.wide[,-seq(4)]=apply(ecotaxa.phoenix1.wide[,-seq(4)],2,as.character)
ecotaxa.phoenix1.wide[,-seq(4)]=apply(ecotaxa.phoenix1.wide[,-seq(4)],2,as.numeric)
row.names(ecotaxa.phoenix1.wide)=substr(ecotaxa.phoenix1.wide$orig_id,13,18)

ecotaxa.phoenix1.pca = PCA(ecotaxa.phoenix1.wide[,-seq(4)], scale.unit=TRUE, 
                           ncp=12, graph=T)

plot.PCA(ecotaxa.phoenix1.pca, axes=c(1, 2), choix="ind")
plot.PCA(ecotaxa.phoenix1.pca, axes=c(2, 3), choix="ind")

ecotaxa.phoenix1.pca$eig

ecotaxa.phoenix1.pca.dimdesc=dimdesc(ecotaxa.phoenix1.pca, axes=c(1,2,3))
names(ecotaxa.phoenix1.pca.dimdesc)

ecotaxa.phoenix1.pca.dimdesc1=data.frame(ecotaxa.phoenix1.pca.dimdesc$Dim.1$quanti)
plot(ecotaxa.phoenix1.pca.dimdesc1$correlation)
ecotaxa.phoenix1.pca.dimdesc1[ecotaxa.phoenix1.pca.dimdesc1$correlation>=.6,]

ecotaxa.phoenix1.pca.dimdesc2=data.frame(ecotaxa.phoenix1.pca.dimdesc$Dim.2$quanti)
plot(ecotaxa.phoenix1.pca.dimdesc2$correlation)
ecotaxa.phoenix1.pca.dimdesc2[ecotaxa.phoenix1.pca.dimdesc2$correlation>=.6,]

ecotaxa.phoenix1.pca.dimdesc3=data.frame(ecotaxa.phoenix1.pca.dimdesc$Dim.3$quanti)
plot(ecotaxa.phoenix1.pca.dimdesc3$correlation)
ecotaxa.phoenix1.pca.dimdesc3[ecotaxa.phoenix1.pca.dimdesc3$correlation>=.6,]

plot(ecotaxa.phoenix1.wide$longitude,ecotaxa.phoenix1.wide$latitude)
dfs=ecotaxa.phoenix1.wide[row.names(ecotaxa.phoenix1.wide)%in%c('WP2_13','WP2_2'),]
points(dfs$longitude,dfs$latitude,pch=16,col=2)

# summary df
ecotaxa.phoenix1.pcas=ecotaxa.phoenix1.pca$ind$coord
ecotaxa.phoenix1.pcas=merge(ecotaxa.phoenix1.pcas,ecotaxa.phoenix1.wide[,seq(4)],
                            by.x='row.names',by.y='row.names')
head(ecotaxa.phoenix1.pcas)
names(ecotaxa.phoenix1.pcas)[1]='Station'

# Stations clustering with HAC ------
ecotaxa.phoenix1.pca.hcpc = HCPC(ecotaxa.phoenix1.pca)
names(ecotaxa.phoenix1.pca.hcpc)
ecotaxa.phoenix1.pca.hcpc$desc.var

ecotaxa.phoenix1.pcas.clust=merge(ecotaxa.phoenix1.pcas,
                                  ecotaxa.phoenix1.pca.hcpc$data.clust,
                            by.x='Station',by.y='row.names')
head(ecotaxa.phoenix1.pcas.clust)

plot(ecotaxa.phoenix1.pcas.clust$longitude,
     ecotaxa.phoenix1.pcas.clust$latitude,
     col=ecotaxa.phoenix1.pcas.clust$clust,
     pch=as.numeric(as.character(
       ecotaxa.phoenix1.pcas.clust$clust)))

plotellipses(res.pca)

# piecharts by PCA: 
ggplot()+geom_scatterpie(aes(x=longitude, y=latitude), 
                         data = ecotaxa.phoenix1.pcas, 
                         cols=paste('Dim.',seq(11),sep=''))
#+annotation_map(map_data("world"))+ #Add the map as a base layer before the points
#  coord_quickmap()  #Sets aspect ratio
ggplot()+geom_scatterpie(aes(x=longitude, y=latitude), 
                         data = ecotaxa.phoenix1.pcas, 
                         cols=paste('Dim.',seq(3),sep=''))

# piecharts by taxon: too many ----------
# add coasline to map
ggplot()+geom_scatterpie(aes(x=longitude, y=latitude), 
                 data = ecotaxa.phoenix1, 
                 cols="display_name", long_format=TRUE)+
  annotation_map(map_data("world"))+ #Add the map as a base layer before the points
  coord_quickmap()  #Sets aspect ratio

# crop worldmap
worldmap <- ne_countries(scale = 'medium', type = 'map_units',
                         returnclass = 'sf')
echosondemap <- st_crop(worldmap, xmin = -3.5, xmax = -2,
                          ymin = 47, ymax = 47.5)
ggplot() + geom_sf(data = echosondemap) + theme_bw()+
  geom_scatterpie(aes(x=longitude, y=latitude),data = ecotaxa.phoenix1,
                  cols="display_name", long_format=TRUE)

dat <- getNOAA.bathy(-3.5,-2,47,47.5,res=1, keep=TRUE)

# Plot bathy object using custom ggplot2 functions: does not work (?)
autoplot(dat, geom=c("r", "c"), colour="white", size=0.1) + scale_fill_etopo()

echosonde.gebco=readGEBCO.bathy('
/media/mathieu/IfremerMData/PHOENIX2018/Data/Bathy/GEBCO_22_Sep_2021_ec879f36e0cf/gebco_2021_n47.476043701171875_s47.036590576171875_w-3.302764892578125_e-2.237091064453125.nc')

autoplot(echosonde.gebco, geom=c("r", "c")) + 
  scale_fill_etopo()+geom_scatterpie(aes(x=longitude, y=latitude), 
                                     data = ecotaxa.phoenix1, 
                                     cols="display_name", long_format=TRUE)

