#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 12 14:16:08 2021

@author: mathieu.doray@ifremer.fr
"""

from pathlib import Path
import ctd
import matplotlib.pyplot as plt
import os

path=Path('/media/mathieu/IfremerMData/PHOENIX2018/CTD/cnv_avg/')
listOfFiles = os.listdir(path)

# 1 file
fname=path.joinpath('W0342.cnv')
cast = ctd.from_cnv(fname)
down, up = ctd.from_cnv(fname).split()
down.columns
ax = down['tv290C'].plot_cast()

# get cast metadata
metadata = cast._metadata
print(metadata['header'])
metadata['lon']

# import several files and plot

def make_patch_spines_invisible(ax):
    ax.set_frame_on(True)
    ax.patch.set_visible(False)
    for sp in ax.spines.values():
        sp.set_visible(False)

# plot TS diagrams

for entry in listOfFiles:
    fname=path.joinpath(entry)
    cast = ctd.from_cnv(fname)
    metadata = cast._metadata 
    
    down, up = ctd.from_cnv(fname).split()
    fig, ax0 = plt.subplots(figsize=(5, 9))
    colors = ['red', 'blue', 'orange','green']
    
    ax0.invert_yaxis()
    ax1 = ax0.twiny()
    ax2 = ax0.twiny()
    ax3 = ax0.twiny()
    
    l0, = ax0.plot(down['tv290C'], down.index, color=colors[0], label='Temperature')
    ax0.set_xlabel('Temperature (°C)')
    
    l1, = ax1.plot(down['sal00'], down.index, color=colors[1], label='Salinity')
    ax1.set_xlabel('Salinity (g kg$^{-1}$)')
    
    l2, = ax2.plot(down['sbeox0ML/L'], down.index, color=colors[2], label='Oxygen')
    ax2.set_xlabel('Oxygen (ML/L)')
    
    l3, = ax3.plot(down['wetStar'], down.index, color=colors[3], label='Chl-a')
    ax3.set_xlabel('Chl-a (mg/L)')
    
    make_patch_spines_invisible(ax2)
    ax2.spines['top'].set_position(('axes', 1.1))
    ax2.spines['top'].set_visible(True)
    
    make_patch_spines_invisible(ax3)
    ax3.spines['top'].set_position(('axes', 1.2))
    ax3.spines['top'].set_visible(True)
    
    ax0.xaxis.label.set_color(l0.get_color())
    ax1.xaxis.label.set_color(l1.get_color())
    ax2.xaxis.label.set_color(l2.get_color())
    ax3.xaxis.label.set_color(l3.get_color())
    
    ax0.tick_params(axis='x', colors=l0.get_color())
    ax1.tick_params(axis='x', colors=l1.get_color())
    ax2.tick_params(axis='x', colors=l2.get_color())
    ax3.tick_params(axis='x', colors=l3.get_color())
    
    lines = ax0.get_lines() + ax1.get_lines() + ax2.get_lines() + ax3.get_lines()
    leg = {
        line: line.get_label() for line in lines
    }
    
    ax0.legend(leg.keys(), leg.values(), loc=8)
    
    ax0.grid(False)
    ax1.grid(False)
    ax2.grid(False)
    ax3.grid(False)
    
    fig.savefig(fname=str(path)+'/'+metadata['name']+'_CTD.png',
                bbox_inches = 'tight',dpi=300)

# plot CTD positions
fig2, ax0 = plt.subplots(figsize=(5, 9))
ax0.axis([-2,-3.5, 47, 47.4])
for entry in listOfFiles:
    fname=path.joinpath(entry)
    cast = ctd.from_cnv(fname)
    metadata = cast._metadata 
    ax0.text(metadata['lon'],metadata['lat'],metadata['name'])
    
fig2.savefig(fname=str(path)+'/CTDpositions.png',bbox_inches = 'tight',dpi=300)












